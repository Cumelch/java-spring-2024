package ru.itis.webflux.slowmoovie;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/all")
public class SlowMoovieController {

    @GetMapping()
    public List<Moovie> slowMoovie() throws InterruptedException {
        Thread.sleep(10000);
        return List.of( new Moovie("Drive"), new Moovie("Barbie"));
    }
}
