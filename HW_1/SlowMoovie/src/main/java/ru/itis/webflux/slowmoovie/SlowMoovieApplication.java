package ru.itis.webflux.slowmoovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlowMoovieApplication {

    public static void main(String[] args) {SpringApplication.run(SlowMoovieApplication.class, args);
    }

}
