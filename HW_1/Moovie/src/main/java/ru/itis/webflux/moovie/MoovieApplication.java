package ru.itis.webflux.moovie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoovieApplication.class, args);
	}

}
