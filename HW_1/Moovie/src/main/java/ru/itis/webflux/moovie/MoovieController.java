package ru.itis.webflux.moovie;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/all")
public class SmartController {

    @GetMapping()
    public List<Moovie> moovie() {
        return List.of( new Moovie("Drive"), new Moovie("Barbie"));
    }
}
